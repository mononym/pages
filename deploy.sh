#!/bin/sh
# source: https://github.com/Shadowsong27/Shadowsong27.github.io/blob/faebc43fef3af3d14dfd9bcf01ef875367059080/deploy.sh

if [[ $(git status -s) ]]
then
    echo "The working directory is dirty. Please commit any pending changes."
    exit 1;
fi

echo "Deleting old publication"
rm -rf public
mkdir public
git worktree prune
rm -rf .git/worktrees/public/

echo "Checking out codeberg-pages branch into public"
git worktree add -B main public origin/main

echo "Removing existing files"
rm -rf public/*

echo "Generating site"
hugo

echo "Copy .domains"
cp .domains public/.domains

echo "Updating main branch"
cd public && git add --all && git commit -m "Publishing to codeberg pages main branch"

git push --force

echo "Updating hugo branch"
cd ../
git push
