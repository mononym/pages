---
_build:
  render: false
cascade:
  _build:
    list: true
    render: false
date: "2022-02-24"
tags: "code"
---
[pohlke](https://codeberg.org/AlICe_lab/pohlke) // v1.1 release // Create canonical axonometric and oblique projection cameras. Add-on for Blender 3.