---
_build:
  render: false
cascade:
  _build:
    list: true
    render: false
date: "2023-03-27"
tags: "talk"
---
[EPFL | ENAC | IA | ALICE](https://www.epfl.ch/labs/alice/) // Projecting with Code, a Pen Plotter and Some History // Lausanne, 27 March 2023.