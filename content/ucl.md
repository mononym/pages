---
_build:
  render: false
cascade:
  _build:
    list: true
    render: false
date: "2023-01-24"
tags: "talk"
---
[UCLouvain | tsa-lab](http://www.tsa-lab.be/) // Séminaire de recherche autour du numérique en architecture // Bruxelles, 24 January 2023.