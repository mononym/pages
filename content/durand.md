---
_build:
  render: false
cascade:
  _build:
    list: true
    render: false
date: "2015-09-01"
tags: "master thesis"
---
[Jean-Nicolas-Louis Durand et la représentation comme instrument](https://cibleplus.ulb.ac.be/permalink/32ULDB_U_INST/1hd430l/alma991009669563804066) -- Architecture. Université libre de Bruxelles, Faculté d’architecture La Cambre-Horta.
