---
_build:
  render: false
cascade:
  _build:
    list: true
    render: false
date: "2024-09-08"
tags: "talk"
---
[Blender Conference 2024](https://conference.blender.org/2024/) // The Missing Camera or: How I Learned to Stop Worrying and Love Oblique Projection // Amsterdam, 23-25 October 2024.
