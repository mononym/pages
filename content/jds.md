---
_build:
  render: false
cascade:
  _build:
    list: true
    render: false
date: "2018-09-13"
tags: "talk"
---
[University of Antwerp](https://www.uantwerpen.be/nl/overuantwerpen/faculteiten/ontwerpwetenschappen/nieuws-en-activiteiten/archief/jds-2018/) // 32nd Joint Doctoral Seminar: History and Theory of Architecture // Antwerpen, 13
September 2018.