---
_build:
  render: false
cascade:
  _build:
    list: true
    render: false
date: "2024-08-23"
tags: "code"
---
[very modest theme](https://codeberg.org/mononym/discourse-very-modest-theme) // v0.1.1 release //  A most minimal theme for Discourse.