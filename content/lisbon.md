---
_build:
  render: false
cascade:
  _build:
    list: true
    render: false
date: "2019-10-18"
tags: "exhibition"
---
[Perspectiva Virtualis](https://codeberg.org/mononym/perspectiva-virtualis) @ [Artificial Realities](http://istar.iscte-iul.pt/artificialrealities/) // [Lisbon Architecture Triennale](https://www.trienaldelisboa.com/) Associated Project. ISTAR—Information Sciences and Technologies and Architecture Research Centre, ISCTE—University Institute of Lisbon, 14-18 October 2019.