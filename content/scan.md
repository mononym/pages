---
_build:
  render: false
cascade:
  _build:
    list: true
    render: false
date: "2020-11-20"
tags: "paper"
---
[“La projection intégrée au Sketchpad III: origine en devenir d’un terrain critique de l’infographie"](https://www.shs-conferences.org/articles/shsconf/abs/2020/10/shsconf_scan2020_01004/shsconf_scan2020_01004.html) in _Culture numérique et conception architecturale... Retour vers le Futur_, 9ème Séminaire de Conception Architecturale Numérique (SCAN), 16-20 Novembre 2020, Faculté d'Architecture La Cambre Horta, Université Libre de Bruxelles.