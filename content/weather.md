---
_build:
  render: false
cascade:
  _build:
    list: true
    render: false
date: "2024-03-12"
tags: "exhibition"
---
Weather Prediction, Capturing National Oceanic and Atmospheric Administration's Satellite Signal /w Truant School & Max Bondu @ [HIGH TECH LOW TECH](https://www.epfl.ch/campus/art-culture/museum-exhibitions/archizoom/zombie-tech-2/) // [Archizoom](https://www.epfl.ch/campus/art-culture/museum-exhibitions/archizoom/) EPFL, 12 March - 13 May 2024.