---
_build:
  render: false
cascade:
  _build:
    list: true
    render: false
date: "2018-09-06"
tags: "talk"
---
[ULB | HABITER](https://ulbhabiter.hypotheses.org/637) // PhD Seminar with special guest interlocutor Murray Fraser – Barlett School of Architecture & University College of London // Bruxelles, 6 September 2018.