---
_build:
  render: false
cascade:
  _build:
    list: false
    render: false
date: "2020-12-07"
tags: "code"
unlisted: true
---
[Perspectiva Virtualis](https://codeberg.org/mononym/perspectiva-virtualis) // 
AR installation to explore the combination of perspective projection and architectural representation.