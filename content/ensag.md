---
_build:
  render: false
cascade:
  _build:
    list: true
    render: false
date: "2023-05-22"
tags: "talk"
---
[ENSAG | MHA](https://www.grenoble.archi.fr/) // Journée d'Étude "Surfaces Opératoires ; Image, Trace, Texte" // Grenoble, 22 May 2023.