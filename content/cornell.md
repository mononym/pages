---
_build:
  render: false
cascade:
  _build:
    list: false
    render: false
date: "2018-10-10"
tags: "talk"
unlisted: true
---
[Cornell University](http://www.dcaconference2018.org/) // 2018 Design Communication Conference // Ithaca, New York, 7-10 October 2018.