---
_build:
  render: false
cascade:
  _build:
    list: true
    render: false
date: "2021-06-26"
tags: "code"
---
[Noise](https://codeberg.org/mononym/noise) // video game developed during the [VOLUPTAS](https://charbonnet-heiz.arch.ethz.ch/) Summer School "World at Play,  Cosmogony vol.I" // Professur Charbonnet/Heiz (ETH Zurich D-ARCH) // June 21-26 2021.