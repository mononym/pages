---
_build:
  render: false
cascade:
  _build:
    list: true
    render: false
date: "2019-04-28"
tags: "talk"
---
[Stiftung Bibliothek Werner Oechslin](https://www.bibliothek-oechslin.ch/) // 8. Architekturtheoretisches Kolloquium : “Firmitas / aedificatio” – die materiellen “körperlichen” Grund­lagen der (gebauten) Architektur // Einsiedeln, 25–28 April 2019.