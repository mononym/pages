---
_build:
  render: false
cascade:
  _build:
    list: true
    render: false
date: "2018-10-10"
tags: "paper"
---
[“Jean-Nicolas-Louis Durand’s Clockwork: Computational Magnification of a 19th Century Procedural Design Mechanism”](https://dipot.ulb.ac.be/dspace/bitstream/2013/283953/3/Pages_556-563_from_DCA2018_proceedings.pdf) in _Virtual + Actual: Process and Product of Design_, 2018 Design Communication Conference, 7-10 October 2018, Cornell University, Ithaca, New York, Saleh Uddin, So-Yeon Yoon, Lora Kim, Christopher Welty.