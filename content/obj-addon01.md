---
_build:
  render: false
cascade:
  _build:
    list: true
    render: false
date: "2023-01-26"
tags: "code"
unlisted: true
---
[object-file-attachments](https://codeberg.org/AlICe_lab/object-file-attachments) // v1.0 release // 
Blender addon to link files to 3D objects.