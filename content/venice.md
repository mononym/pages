---
_build:
  render: false
cascade:
  _build:
    list: true
    render: false
date: "2017-11-01"
tags: "work"
---
Collaborator of the [Fondazione La Biennale di Venezia](https://www.labiennale.org) for the 15th International Architecture Exhibition “Reporting from the Front” by Alejandro Aravena (2016) and 57th International Art Exhibition “Viva Arte Viva” by Christine Macel (2017).