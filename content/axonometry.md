---
_build:
  render: false
cascade:
  _build:
    list: true
    render: false
date: "2022-03-08"
tags: "code"
---
[axonometry](https://codeberg.org/mononym/axonometry) // axonometry by intersection - XIX' c. style.
