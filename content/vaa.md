---
_build:
  render: false
cascade:
  _build:
    list: true
    render: false
date: "2021-10-17"
tags: "talk"
---
[Vilnius Academy of Arts](https://www.vda.lt/en/) // To Research or Not To Research in the Post-disciplinary Academy ? _ X-disciplinary Congress on Artistic Research and Related Matters // Vilnius, 14-17 October 2021.