---
_build:
  render: false
cascade:
  _build:
    list: false
    render: false
date: "2020-11-20"
tags: "talk"
unlisted: true
---
[Université libre de Bruxelles](https://archi.ulb.be) // Culture numérique et conception architecturale... Retour vers le Futur _ 9ème Séminaire de Conception Architecturale Numérique (SCAN) // Bruxelles, 16-20 November 2020.