---
_build:
  render: false
cascade:
  _build:
    list: true
    render: false
date: "2023-02-02"
tags: "code"
---
[object-file-attachments](https://codeberg.org/AlICe_lab/object-file-attachments) // v1.1 release // 
Blender addon to link files to 3D objects.