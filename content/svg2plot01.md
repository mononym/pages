---
_build:
  render: false
cascade:
  _build:
    list: true
    render: false
date: "2022-03-08"
tags: "code"
---
[svg2plot](https://codeberg.org/mononym/svg2plot) // CLI to convert svg files to gcode and send them via usb serial port to your xy-plotter.
