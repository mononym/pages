---
_build:
  render: false
cascade:
  _build:
    list: true
    render: false
date: "2022-06-21"
tags: "workshop"
---
Perspectives on Dwelling : Architectural Anthropologies of Home with [Ray Lucas](https://www.msa.ac.uk/staff/rlucas/), Manchester School of Architecture, ULB International Chair 2022.