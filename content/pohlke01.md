---
_build:
  render: false
cascade:
  _build:
    list: true
    render: false
date: "2021-12-31"
tags: "code"
unlisted: true
---
[pohlke](https://codeberg.org/AlICe_lab/pohlke) // v1.0 pre-release // Create canonical axonometric and oblique projection cameras. Add-on for Blender 3.
