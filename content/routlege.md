---
_build:
  render: false
cascade:
  _build:
    list: true
    render: false
date: "2021-08-31"
tags: "chapter"
---
[“Perspectiva Virtualis”](https://www.taylorfrancis.com/chapters/edit/10.4324/9781003183105-38/perspectiva-virtualis-julien-rippinger-arthur-lachard) in _Virtual Aesthetics in Architecture: Designing in Mixed Realities_, w/ Arthur Lachard, edited by Sara Eloy, Anette Kreutzberg, and Ioanna Symenidou. New York: Routledge, 2021.