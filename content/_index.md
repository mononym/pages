---
title: 'home'
---
{{< rawhtml >}}
<ul class="list">
<li>
Julien Rippinger<br>ლ(´ڡ`ლ)
</li>
<li><a class="tag-link" href="./tags/network/"><stamp>[network]</stamp></a>
<p>Hello. This is my site.<br>
I’m a PhD candidate within the <a href="http://alicelab.be">AlICe</a> research laboratory at the Université libre de Bruxelles.
My work is supported by the <a href="https://www.fnr.lu/">Luxembourg National Research Fund</a> and the Auguste van Werveke-Hanno Foundation.</p>
</li>
{{< /rawhtml >}}
