---
_build:
  render: false
cascade:
  _build:
    list: true
    render: false
date: "2024-07-02"
tags: "code"
---
[procedural model archive](https://codeberg.org/AlICe_lab/procedural-model-archive) // v0.1.0 release
